package com.learn.mobileappws.io.repository;

import com.learn.mobileappws.model.request.UserDetailRequestModel;
import com.learn.mobileappws.model.response.UserRest;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigInteger;
import java.util.List;

@Repository
@Transactional
public class SampleRepository {

    public SampleRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @PersistenceContext
    private final EntityManager entityManager;


    public UserRest buildQuery(){
        String query="select * from users";
        Query q = entityManager.createNativeQuery(query);


//        List<UserRest> results = entityManager.createNativeQuery
//                ("SELECT * FROM users", UserRest.class).getResultList();
//        System.out.println(results);
      //  em.createNativeQuery("SELECT * FROM book b WHERE id = 1", "BookMapping").getSingleResult();

        UserRest ur = new UserRest();

        List<Object[]> results = this.entityManager.createNativeQuery("SELECT * FROM users").getResultList();


        results.stream().forEach((record) -> {


            String id = (String)  record[7];
            ur.setUserId(id);
            String firstName = (String) record[5];
            ur.setFirstName(firstName);
            String lastName = (String) record[6];
            ur.setLastName(lastName);
            String email = (String) record[1];
            ur.setEmail(email);
        });

        return ur;

    }


    public UserRest buildUpdateQuery(int id) {

        String query="update users u set u.EMAIL = 'soma@gmail' where u.ID =:id";
         this.entityManager.createNativeQuery(query);

        UserRest ur = new UserRest();

        List<Object[]> results = this.entityManager.createNativeQuery("SELECT * FROM users where ID=id").getResultList();


        results.stream().forEach((record) -> {


            String iid = (String)  record[7];
            ur.setUserId(iid);
            String firstName = (String) record[5];
            ur.setFirstName(firstName);
            String lastName = (String) record[6];
            ur.setLastName(lastName);
            String email = (String) record[1];
            ur.setEmail(email);
        });


        System.out.println(ur.toString());
        return ur;

    }


    public void updateUser(String email){
        int i = entityManager.createQuery("update users set email ='eeee' where id=1").executeUpdate();
        entityManager.getTransaction().commit();

    }
}
