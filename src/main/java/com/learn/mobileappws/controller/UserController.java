package com.learn.mobileappws.controller;

import com.learn.mobileappws.dao.*;
import com.learn.mobileappws.io.repository.SampleRepository;
import com.learn.mobileappws.io.repository.UserRepository;
import com.learn.mobileappws.model.request.*;
import com.learn.mobileappws.model.response.*;
import com.learn.mobileappws.service.*;
import org.springframework.beans.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/users")// http://localhost:8080/
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;

//    @Value("${h2.username}")
//    String value;




    @Autowired
    SampleRepository sampleRepository;
    @PostMapping(value = "/createuser", consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}, //http://localhost:8080/users/createuser
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public UserRest createUser(@RequestBody UserDetailRequestModel userDetails) {
        UserRest returnValue = new UserRest();
      //  System.out.println("========value is "+value);
        UserDao userDao = new UserDao();
        BeanUtils.copyProperties(userDetails, userDao);
        UserDao createdUser = userService.createUser(userDao);
        BeanUtils.copyProperties(createdUser, returnValue);
        return returnValue;
    }

    @GetMapping(path = "/getuser/{id}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    //http://localhost:8080/users/getuser/2mso

    public UserRest getUser(@PathVariable String id) {
        UserRest returnValue = new UserRest();
        UserDao userDao = userService.getUserByUserId(id);
        BeanUtils.copyProperties(userDao, returnValue);
        return returnValue;
    }

    @GetMapping(path = "/getallusers", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    //http://localhost:8080/users/getallusers?page=0&limit=5

    public List<UserRest> getAllUsers(@RequestParam(value = "page", defaultValue = "0") int page, @RequestParam(value = "limit", defaultValue = "25") int limit) {
        List<UserRest> returnValue = new ArrayList<>();
        List<UserDao> users = userService.getUsers(page, limit);
        for (UserDao userDao : users) {
            UserRest userModel = new UserRest();
            BeanUtils.copyProperties(userDao, userModel);
            returnValue.add(userModel);
        }
        return returnValue;
    }

    @PutMapping(path = "updateuser/{id}", consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}, //http://localhost:8080/users/updateuser/qmig
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public UserRest updateUser(@PathVariable String id, @RequestBody UserDetailRequestModel uderDetails) {
        UserRest rerurnValue = new UserRest();
        UserDao userDao = new UserDao();
        BeanUtils.copyProperties(uderDetails, userDao);
        UserDao updateUser = userService.updateUser(id, userDao);
        BeanUtils.copyProperties(updateUser, rerurnValue);
        return rerurnValue;
    }

    @DeleteMapping(path = "deleteuser/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE}, //http://localhost:8080/users/deleteuser/kma0
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public OperationStatusModel deleteUser(@PathVariable String id) {
        OperationStatusModel returnValue = new OperationStatusModel();
        returnValue.setOperationName(RequestOperationName.DELETE.name());
        userService.deleteUserByUserId(id);
        returnValue.setOperationResult(RequestOperationStatus.SUCCESS.name());
        return returnValue;
    }


    @GetMapping
    public UserRest getNative(){

       UserRest obj= sampleRepository.buildQuery();

        return obj;


    }


    @PutMapping("/{id}")
    public void getUpdate(){


         sampleRepository.updateUser("sumaaa@ddd.com");



    }


}
