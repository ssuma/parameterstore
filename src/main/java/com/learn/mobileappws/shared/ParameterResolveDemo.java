package com.learn.mobileappws.shared;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ParameterResolveDemo implements CommandLineRunner {

    @Value("${h2.username}")
    private String message;

    @Value("${h2.password}")
    private String dataSourcePassword;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Message property: " + message);
        System.out.println("DataSource password property: " + dataSourcePassword);
    }
}
